import * as THREE from "https://cdn.jsdelivr.net/npm/three@0.124.0/build/three.module.js";
import { OrbitControls } from "https://cdn.jsdelivr.net/npm/three@0.124.0/examples/jsm/controls/OrbitControls.js";
import rhino3dm from "https://cdn.jsdelivr.net/npm/rhino3dm@7.15.0/rhino3dm.module.min.js";
import { STLExporter } from "https://cdn.jsdelivr.net/npm/three@0.124.0/examples/jsm/exporters/STLExporter.js";

/* eslint no-undef: "off", no-unused-vars: "off" */

const definition = "KlavenessLastv8.gh";

const exporter = new STLExporter();
var zipper = new JSZip();

var SizeMode = "EU";
updateSizeSelect();

var InsoleType = "Full";

const SyncLRCB = document.getElementById("SyncLeftRightCB");
SyncLRCB.addEventListener("change", (event) => {
  onSyncEnable();
});

// setup input change events for Left
var LeftFifthToeClearanceSlider = new Slider("#LeftFifthToeClearance");
LeftFifthToeClearanceSlider.on("slideStop", function (sliderValue) {
  document.getElementById("LeftFifthToeClearanceSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    RightFifthToeClearanceSlider.setValue(sliderValue);
    document.getElementById("RightFifthToeClearanceSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var LeftHalluxValgusSlider = new Slider("#LeftHalluxValgus");
LeftHalluxValgusSlider.on("slideStop", function (sliderValue) {
  document.getElementById("LeftHalluxValgusSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    RightHalluxValgusSlider.setValue(sliderValue);
    document.getElementById("RightHalluxValgusSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var LeftHammerToesSlider = new Slider("#LeftHammerToes");
LeftHammerToesSlider.on("slideStop", function (sliderValue) {
  document.getElementById("LeftHammerToesSliderVal").textContent = sliderValue;
  if (SyncLRCB.checked) {
    RightHammerToesSlider.setValue(sliderValue);
    document.getElementById("RightHammerToesSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var LeftInstepSlider = new Slider("#LeftInstep");
LeftInstepSlider.on("slideStop", function (sliderValue) {
  document.getElementById("LeftInstepSliderVal").textContent = sliderValue;
  if (SyncLRCB.checked) {
    RightInstepSlider.setValue(sliderValue);
    document.getElementById("RightInstepSliderVal").textContent = sliderValue;
  }
  onSliderChange();
});

var LeftDepthAdjustmentSlider = new Slider("#LeftDepthAdjustment");
LeftDepthAdjustmentSlider.on("slideStop", function (sliderValue) {
  document.getElementById("LeftDepthAdjustmentSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    RightDepthAdjustmentSlider.setValue(sliderValue);
    document.getElementById("RightDepthAdjustmentSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var LeftHeelWidthSlider = new Slider("#LeftHeelWidthSlider");
LeftHeelWidthSlider.on("slideStop", function (sliderValue) {
  document.getElementById("LeftHeelWidthSliderVal").textContent = sliderValue;
  if (SyncLRCB.checked) {
    RightHeelWidthSlider.setValue(sliderValue);
    document.getElementById("RightHeelWidthSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var LeftForefootWidthSlider = new Slider("#LeftForefootWidthSlider");
LeftForefootWidthSlider.on("slideStop", function (sliderValue) {
  document.getElementById("LeftForefootWidthSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    RightForefootWidthSlider.setValue(sliderValue);
    document.getElementById("RightForefootWidthSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

// setup input change events for Right
var RightFifthToeClearanceSlider = new Slider("#RightFifthToeClearance");
RightFifthToeClearanceSlider.on("slideStop", function (sliderValue) {
  document.getElementById("RightFifthToeClearanceSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    LeftFifthToeClearanceSlider.setValue(sliderValue);
    document.getElementById("LeftFifthToeClearanceSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var RightHalluxValgusSlider = new Slider("#RightHalluxValgus");
RightHalluxValgusSlider.on("slideStop", function (sliderValue) {
  document.getElementById("RightHalluxValgusSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    LeftHalluxValgusSlider.setValue(sliderValue);
    document.getElementById("LeftHalluxValgusSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var RightHammerToesSlider = new Slider("#RightHammerToes");
RightHammerToesSlider.on("slideStop", function (sliderValue) {
  document.getElementById("RightHammerToesSliderVal").textContent = sliderValue;
  if (SyncLRCB.checked) {
    LeftHammerToesSlider.setValue(sliderValue);
    document.getElementById("LeftHammerToesSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var RightInstepSlider = new Slider("#RightInstep");
RightInstepSlider.on("slideStop", function (sliderValue) {
  document.getElementById("RightInstepSliderVal").textContent = sliderValue;
  if (SyncLRCB.checked) {
    LeftInstepSlider.setValue(sliderValue);
    document.getElementById("LeftInstepSliderVal").textContent = sliderValue;
  }
  onSliderChange();
});

var RightDepthAdjustmentSlider = new Slider("#RightDepthAdjustment");
RightDepthAdjustmentSlider.on("slideStop", function (sliderValue) {
  document.getElementById("RightDepthAdjustmentSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    LeftDepthAdjustmentSlider.setValue(sliderValue);
    document.getElementById("LeftDepthAdjustmentSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

var RightHeelWidthSlider = new Slider("#RightHeelWidthSlider");
RightHeelWidthSlider.on("slideStop", function (sliderValue) {
  document.getElementById("RightHeelWidthSliderVal").textContent = sliderValue;
  if (SyncLRCB.checked) {
    LeftHeelWidthSlider.setValue(sliderValue);
    document.getElementById("LeftHeelWidthSliderVal").textContent = sliderValue;
  }
  onSliderChange();
});

var RightForefootWidthSlider = new Slider("#RightForefootWidthSlider");
RightForefootWidthSlider.on("slideStop", function (sliderValue) {
  document.getElementById("RightForefootWidthSliderVal").textContent =
    sliderValue;
  if (SyncLRCB.checked) {
    LeftForefootWidthSlider.setValue(sliderValue);
    document.getElementById("LeftForefootWidthSliderVal").textContent =
      sliderValue;
  }
  onSliderChange();
});

$("#ModelSelect").change(function () {
  onSliderChange();
  updateSizeSelect();
});

$("#SizeSelectEU").change(function () {
  onSliderChange();
});

$("#SizeSelectUK").change(function () {
  onSliderChange();
});

var DownloadSTLBtn = document.getElementById("DownloadSTL");
DownloadSTLBtn.onclick = function (event) {
  SaveInsoles();
};

var DownloadSubDsBtn = document.getElementById("DownloadSubDs");
DownloadSubDsBtn.onclick = function (event) {
  SaveSubDs();
};

var DownloadPDFBtn = document.getElementById("DownloadPDF");
DownloadPDFBtn.onclick = function (event) {
  SavePDF();
};

// load the rhino3dm library
let rhino, subddoc;
rhino3dm().then(async (m) => {
  console.log("Loaded rhino3dm.");
  rhino = m; // global
  init();
  compute();
});

let _threeMesh, _threeMaterial;
let _threeMesh2, _threeMaterial2;

let mesh, mesh2;

/**
 * Call appserver
 */

async function compute() {
  let t0 = performance.now();
  const timeComputeStart = t0;

  // collect data from inputs
  let data = {};
  let foundSize = 0;
  if (SizeMode === "EU") {
    foundSize = parseInt($("#SizeSelectEU").val());
  } else {
    foundSize = parseInt($("#SizeSelectUK").val());
  }

  data.definition = definition;
  data.inputs = {
    ModelID: parseInt($("#ModelSelect").val()),
    ModelSize: foundSize,

    LeftFifthToeClearance: LeftFifthToeClearanceSlider.getValue(),
    LeftHalluxValgus: LeftHalluxValgusSlider.getValue(),
    LeftHammerToes: LeftHammerToesSlider.getValue(),
    LeftInstep: LeftInstepSlider.getValue(),
    LeftDepthAdjustment: LeftDepthAdjustmentSlider.getValue(),
    LeftHeelWidth: LeftHeelWidthSlider.getValue(),
    LeftForefootWidth: LeftForefootWidthSlider.getValue(),
    RightFifthToeClearance: RightFifthToeClearanceSlider.getValue(),
    RightHalluxValgus: RightHalluxValgusSlider.getValue(),
    RightHammerToes: RightHammerToesSlider.getValue(),
    RightInstep: RightInstepSlider.getValue(),
    RightDepthAdjustment: RightDepthAdjustmentSlider.getValue(),
    RightHeelWidth: RightHeelWidthSlider.getValue(),
    RightForefootWidth: RightForefootWidthSlider.getValue(),
  };

  console.log(data.inputs);

  const request = {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-Type": "application/json" },
  };

  let headers = null;

  try {
    const response = await fetch("/solve", request);

    // clear doc
    if (subddoc !== undefined) {
      subddoc.delete();
    }

    if (!response.ok) throw new Error(response.statusText);

    headers = response.headers.get("server-timing");
    const responseJson = await response.json();

    // Request finished. Do processing here.
    let t1 = performance.now();
    const computeSolveTime = t1 - timeComputeStart;
    t0 = t1;

    // hide spinner
    console.log(responseJson);
    document.getElementById("loader").style.display = "none";
    responseJson.values.forEach((value) => {
      switch (value.ParamName) {
        case "LeftMesh":
          let data = JSON.parse(value.InnerTree["{0}"][0].data);
          mesh = rhino.DracoCompression.decompressBase64String(data);
          break;
        case "RightMesh":
          let data2 = JSON.parse(value.InnerTree["{0}"][0].data);
          mesh2 = rhino.DracoCompression.decompressBase64String(data2);
          break;
        default:
        // console.log(`Not using ${value.ParamName}`);
      }
    });

    /*const docstr = responseJson.values[0].InnerTree["{0}"][0].data;
    const docdata = JSON.parse(docstr);
    const docarr = _base64ToArrayBuffer(docdata);
    subddoc = rhino.File3dm.fromByteArray(docarr);

    if (subddoc.objects().count < 1) {
      console.log("No rhino objects to load!");
      return;
    } else {
      console.log(`Found ${subddoc.objects().count} objects`);
    }*/

    t1 = performance.now();
    const decodeMeshTime = t1 - t0;
    t0 = t1;

    if (!_threeMaterial) {
      _threeMaterial = new THREE.MeshPhysicalMaterial({
        color: 0x49ef4,
        metalness: 0,
        roughness: 1,
        side: THREE.DoubleSide,
      });
    }

    if (!_threeMaterial2) {
      _threeMaterial2 = new THREE.MeshPhysicalMaterial({
        color: 0x10e5dc,
        metalness: 0,
        roughness: 1,
        side: THREE.DoubleSide,
      });
    }

    let threeMesh = meshToThreejs(mesh, _threeMaterial);
    mesh.delete();
    replaceCurrentMesh(threeMesh);

    let threeMesh2 = meshToThreejs(mesh2, _threeMaterial);
    mesh2.delete();
    replaceCurrentMesh2(threeMesh2);

    t1 = performance.now();
    const rebuildSceneTime = t1 - t0;
  } catch (error) {
    console.error(error);
  }
}

/**
 * Called when a slider value changes in the UI. Collect all of the
 * slider values and call compute to solve for a new scene
 */
function onSliderChange() {
  //show spinner
  document.getElementById("loader").style.display = "block";
  // console.log("onSliderChanged Triggered");
  compute();
}

function updateSizeSelect() {
  let UKSizes = [3, 4, 5, 8, 9, 10];

  // console.log(UKSizes.includes(parseInt($("#ModelSelect").val())));
  if (UKSizes.includes(parseInt($("#ModelSelect").val()))) {
    // console.log("Updating sizing to UK");
    $("#SizeSelectEU").hide();
    $("#SizeSelectUK").show();
    SizeMode = "UK";
  } else {
    //console.log("Updating sizing to EU");
    $("#SizeSelectEU").show();
    $("#SizeSelectUK").hide();
    SizeMode = "EU";
  }
}

function onSyncEnable() {
  //check which tab is active and then set the deactive values to the corresponding values
  // console.log("onSyncEnable Triggered");
  if (SyncLRCB.checked) {
    // console.log("Initial sync");
    //const activated_pane = document.querySelector(event.target.getAttribute('data-bs-target'))
    const activeTab = document.getElementsByClassName(
      "tab-pane fade show active"
    )[0].id;
    if (activeTab == "leftContent") {
      RightFifthToeClearanceSlider.setValue(
        LeftFifthToeClearanceSlider.getValue()
      );
      document.getElementById("RightFifthToeClearanceSliderVal").textContent =
        LeftFifthToeClearanceSlider.getValue();

      RightHalluxValgusSlider.setValue(LeftHalluxValgusSlider.getValue());
      document.getElementById("RightHalluxValgusSliderVal").textContent =
        LeftHalluxValgusSlider.getValue();

      RightHammerToesSlider.setValue(LeftHammerToesSlider.getValue());
      document.getElementById("RightHammerToesSliderVal").textContent =
        LeftHammerToesSlider.getValue();

      RightInstepSlider.setValue(LeftInstepSlider.getValue());
      document.getElementById("RightInstepSliderVal").textContent =
        LeftInstepSlider.getValue();

      RightDepthAdjustmentSlider.setValue(LeftDepthAdjustmentSlider.getValue());
      document.getElementById("RightDepthAdjustmentSliderVal").textContent =
        LeftDepthAdjustmentSlider.getValue();

      RightHeelWidthSlider.setValue(LeftHeelWidthSlider.getValue());
      document.getElementById("RightHeelWidthSliderVal").textContent =
        LeftHeelWidthSlider.getValue();

      RightForefootWidthSlider.setValue(LeftForefootWidthSlider.getValue());
      document.getElementById("RightForefootWidthSliderVal").textContent =
        LeftForefootWidthSlider.getValue();
    } else if (activeTab == "rightContent") {
      LeftFifthToeClearanceSlider.setValue(
        RightFifthToeClearanceSlider.getValue()
      );
      document.getElementById("LeftFifthToeClearanceSliderVal").textContent =
        RightFifthToeClearanceSlider.getValue();

      LeftHalluxValgusSlider.setValue(RightHalluxValgusSlider.getValue());
      document.getElementById("LeftHalluxValgusSliderVal").textContent =
        RightHalluxValgusSlider.getValue();

      LeftHammerToesSlider.setValue(RightHammerToesSlider.getValue());
      document.getElementById("LeftHammerToesSliderVal").textContent =
        RightHammerToesSlider.getValue();

      LeftInstepSlider.setValue(RightInstepSlider.getValue());
      document.getElementById("LeftInstepSliderVal").textContent =
        RightInstepSlider.getValue();

      LeftDepthAdjustmentSlider.setValue(RightDepthAdjustmentSlider.getValue());
      document.getElementById("LeftDepthAdjustmentSliderVal").textContent =
        RightDepthAdjustmentSlider.getValue();

      LeftHeelWidthSlider.setValue(RightHeelWidthSlider.getValue());
      document.getElementById("LeftHeelWidthSliderVal").textContent =
        RightHeelWidthSlider.getValue();

      LeftForefootWidthSlider.setValue(RightForefootWidthSlider.getValue());
      document.getElementById("LeftForefootWidthSliderVal").textContent =
        RightForefootWidthSlider.getValue();
    }
    onSliderChange();
  }
}

// BOILERPLATE //

var scene, camera, renderer, controls, pointlight;

function init() {
  // Rhino models are z-up, so set this as the default
  THREE.Object3D.DefaultUp = new THREE.Vector3(0, 0, 1);

  scene = new THREE.Scene();
  scene.background = new THREE.Color("rgb(245,245,245)");

  renderer = new THREE.WebGLRenderer({
    antialias: true,
    preserveDrawingBuffer: true,
  });
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;
  document.getElementById("threedwrapper").appendChild(renderer.domElement);

  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    2000
  );
  camera.position.set(-300, 300, 250);
  camera.lookAt(0, 0, 0);
  controls = new OrbitControls(camera, renderer.domElement);

  controls.enableDamping = true;

  var ambientLight = new THREE.AmbientLight(0xcccccc, 0.5);
  scene.add(ambientLight);

  const hemisphereLight = new THREE.HemisphereLight(0xff0000, 0x0000ff, 0.7);
  scene.add(hemisphereLight);

  const dirLight = new THREE.DirectionalLight(0xffffff, 0.75);
  dirLight.position.set(0, 0, 300);
  dirLight.castShadow = true;

  dirLight.shadow.mapSize.set(512 * 4, 512 * 4);
  dirLight.shadow.radius = 25;

  dirLight.shadow.camera.left = -500;
  dirLight.shadow.camera.right = 500;
  dirLight.shadow.camera.top = 500;
  dirLight.shadow.camera.bottom = -500;

  dirLight.shadow.camera.near = 1;
  dirLight.shadow.camera.far = 500;

  scene.add(dirLight);

  const planeGeo = new THREE.PlaneGeometry(500, 500, 1, 1);
  const planeMat = new THREE.ShadowMaterial();
  planeMat.opacity = 0.4;

  const plane = new THREE.Mesh(planeGeo, planeMat);
  plane.receiveShadow = true;
  scene.add(plane);

  const gridHelper = new THREE.GridHelper(500, 25, 0xc1c1c1, 0x8d8d8d);
  gridHelper.position.y = -2;
  gridHelper.rotateX(-Math.PI / 2);
  gridHelper.material.transparent = true;
  gridHelper.material.opacity = 0.2;
  scene.add(gridHelper);

  window.addEventListener("resize", onWindowResize, false);

  animate();
}

var animate = function () {
  requestAnimationFrame(animate);
  controls.update();
  renderer.render(scene, camera);
};

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
  animate();
}

function replaceCurrentMesh(threeMesh) {
  if (_threeMesh) {
    scene.remove(_threeMesh);
    _threeMesh.geometry.dispose();
  }
  _threeMesh = threeMesh;
  _threeMesh.castShadow = true;
  scene.add(_threeMesh);
}

function replaceCurrentMesh2(threeMesh) {
  if (_threeMesh2) {
    scene.remove(_threeMesh2);
    _threeMesh2.geometry.dispose();
  }
  _threeMesh2 = threeMesh;
  _threeMesh2.castShadow = true;
  scene.add(_threeMesh2);
}

function meshToThreejs(mesh, material) {
  let loader = new THREE.BufferGeometryLoader();
  var geometry = loader.parse(mesh.toThreejsJSON());
  return new THREE.Mesh(geometry, material);
}

function SaveInsoles() {
  const leftResult = exporter.parse(_threeMesh);
  const rightResult = exporter.parse(_threeMesh2);

  let dateTime = moment().format("YYYYMMDDHHmmss");

  let chosenModel = $("#ModelSelect :selected").text();

  let foundSize = 0;
  if (SizeMode === "EU") {
    foundSize = $("#SizeSelectEU :selected").text();
  } else {
    foundSize = $("#SizeSelectUK :selected").text();
  }

  zipper.file(
    `${dateTime}-${chosenModel}-${foundSize}-Left.stl`,
    new Blob([leftResult], { type: "text/plain" })
  );
  zipper.file(
    `${dateTime}-${chosenModel}-${foundSize}-Right.stl`,
    new Blob([rightResult], { type: "text/plain" })
  );

  zipper.generateAsync({ type: "blob" }).then(function (content) {
    // see FileSaver.js
    saveAs(content, `${dateTime}-${chosenModel}-${foundSize}.zip`);
  });
}

function SaveMeshs() {
  const options = new rhino.File3dmWriteOptions();
  options.version = 7;
  let dateTime = moment().format("YYYYMMDDHHmmss");

  let chosenModel = $("#ModelSelect :selected").text();

  let foundSize = 0;
  if (SizeMode === "EU") {
    foundSize = $("#SizeSelectEU :selected").text();
  } else {
    foundSize = $("#SizeSelectUK :selected").text();
  }

  let buffer = meshdoc.toByteArray();

  //Construct Filename
  //ISODate-ClientID/SessionID(ifPresent)-Model-Size (ie 20240527142713-Free-43.3dm) -

  saveByteArray(`${dateTime}-${chosenModel}-${foundSize}.3dm`, buffer);
}

function SaveSubDs() {
  const options = new rhino.File3dmWriteOptions();
  options.version = 7;
  let buffer = subddoc.toByteArray();

  let dateTime = moment().format("YYYYMMDDHHmmss");

  let chosenModel = $("#ModelSelect :selected").text();

  let foundSize = 0;
  if (SizeMode === "EU") {
    foundSize = $("#SizeSelectEU :selected").text();
  } else {
    foundSize = $("#SizeSelectUK :selected").text();
  }
  saveByteArray(`${dateTime}-${chosenModel}-${foundSize}.3dm`, buffer);
}

function saveByteArray(fileName, byte) {
  let blob = new Blob([byte], { type: "application/octect-stream" });
  let link = document.createElement("a");
  link.href = window.URL.createObjectURL(blob);
  link.download = fileName;
  link.click();
}

function _base64ToArrayBuffer(base64) {
  var binary_string = window.atob(base64);
  var len = binary_string.length;
  var bytes = new Uint8Array(len);
  for (var i = 0; i < len; i++) {
    bytes[i] = binary_string.charCodeAt(i);
  }
  return bytes.buffer;
}

function saveString(text, filename) {
  saveAs(new Blob([text], { type: "text/plain" }), filename);
}

async function SavePDF() {
  let chosenModel = $("#ModelSelect").val();
  let foundSize = 0;

  let chosenModelText = $("#ModelSelect :selected").text();
  let foundSizeText = "";

  if (SizeMode === "EU") {
    foundSizeText = $("#SizeSelectEU :selected").text();
    foundSize = $("#SizeSelectEU").val();
  } else {
    foundSizeText = $("#SizeSelectUK :selected").text();
    foundSize = $("#SizeSelectUK").val();
  }

  let customization = {
    customization: [
      {
        model: chosenModel,
        modelText: chosenModelText,
        size: foundSize,
        sizeText: foundSizeText,

        LeftFifthToeClearance: LeftFifthToeClearanceSlider.getValue(),
        LeftHalluxValgus: LeftHalluxValgusSlider.getValue(),
        LeftHammerToes: LeftHammerToesSlider.getValue(),
        LeftInstep: LeftInstepSlider.getValue(),
        LeftDepthAdjustment: LeftDepthAdjustmentSlider.getValue(),
        LeftHeelWidth: LeftHeelWidthSlider.getValue(),
        LeftForefootWidth: LeftForefootWidthSlider.getValue(),
        RightFifthToeClearance: RightFifthToeClearanceSlider.getValue(),
        RightHalluxValgus: RightHalluxValgusSlider.getValue(),
        RightHammerToes: RightHammerToesSlider.getValue(),
        RightInstep: RightInstepSlider.getValue(),
        RightDepthAdjustment: RightDepthAdjustmentSlider.getValue(),
        RightHeelWidth: RightHeelWidthSlider.getValue(),
        RightForefootWidth: RightForefootWidthSlider.getValue(),

        clientRef: document.getElementById("ClientReferenceInput").value,
        clientComments: document.getElementById("ClientCommentsInput").value,
      },
    ],
  };

  let urlAdd = JSON.stringify(customization);

  const url = "/LastReceipt"; //A local page
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var new_window = window.open();
        new_window.document.write(xhr.response);
      } else {
        // console.log(xhr.statusText);
      }
    }
  };

  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.send(urlAdd);
}

// Download image
function downloadImage(data) {
  let dateTime = moment().format("YYYYMMDDHHmmss");

  let chosenModel = $("#ModelSelect :selected").text();

  let foundSize = 0;
  if (SizeMode === "EU") {
    foundSize = $("#SizeSelectEU :selected").text();
  } else {
    foundSize = $("#SizeSelectUK :selected").text();
  }
  var filename = `${dateTime}-${chosenModel}-${foundSize}.jpg`;
  var a = document.createElement("a");

  var svgElements = document.body.querySelectorAll("svg");
  svgElements.forEach(function (item) {
    item.setAttribute("width", item.getBoundingClientRect().width);
    item.setAttribute("height", item.getBoundingClientRect().height);
    item.style.width = null;
    item.style.height = null;
  });

  let width = document.body.clientWidth;
  let height = document.body.clientHeight;
  let aspectratio = height / width;

  if (aspectratio > 1) {
    var pdf = new jsPDF("p", "mm", "a4");
    pdf.text(10, 10, "Klaveness Last configurator");
    pdf.addImage(data, "JPEG", 10, 20, 190, aspectratio * 190);
    pdf.save("Klaveness Insole Configurator.pdf");
  } else {
    var pdf = new jsPDF("l", "mm", "a4");
    pdf.text(10, 10, "Klaveness Last configurator");
    pdf.addImage(data, "JPEG", 10, 20, 270, aspectratio * 270);
    pdf.save("Klaveness Insole Configurator.pdf");
  }
}
