import * as THREE from "https://cdn.jsdelivr.net/npm/three@0.124.0/build/three.module.js";
import { OrbitControls } from "https://cdn.jsdelivr.net/npm/three@0.124.0/examples/jsm/controls/OrbitControls.js";
import rhino3dm from "https://cdn.jsdelivr.net/npm/rhino3dm@0.15.0-beta/rhino3dm.module.js";

/* eslint no-undef: "off", no-unused-vars: "off" */
const definition = "KlavenessLast.gh";
// load the rhino3dm library

let rhino;
rhino3dm().then(async (m) => {
  console.log("Loaded rhino3dm");
  rhino = m; // global
  init();
  compute();
});
let _threeMesh, _threeMaterial;
let _threeMesh2, _threeMaterial2;
let mesh, mesh2;

/**
 * Call appserver
 */
async function compute() {
  let t0 = performance.now();
  const timeComputeStart = t0;
  // collect data from inputs
  let data = {};
  data.definition = definition;

  data.inputs = {
    "RH_IN:ModelID": document.getElementById("customModelID").textContent,
    "RH_IN:ModelSize": document.getElementById("customSizeID").textContent,

    "RH_IN:LeftFifthToeClearance": document.getElementById(
      "LeftFifthToeClearance"
    ).textContent,
    "RH_IN:LeftHalluxValgus":
      document.getElementById("LeftHalluxValgus").textContent,
    "RH_IN:LeftHammerToes":
      document.getElementById("LeftHammerToes").textContent,
    "RH_IN:LeftInstep": document.getElementById("LeftInstep").textContent,
    "RH_IN:LeftDepthAdjustment": document.getElementById("LeftDepthAdjustment")
      .textContent,
    "RH_IN:LeftHeelWidth": document.getElementById("LeftHeelWidth").textContent,
    "RH_IN:LeftForefootWidth":
      document.getElementById("LeftForefootWidth").textContent,

    "RH_IN:RightFifthToeClearance": document.getElementById(
      "RightFifthToeClearance"
    ).textContent,
    "RH_IN:RightHalluxValgus":
      document.getElementById("RightHalluxValgus").textContent,
    "RH_IN:RightHammerToes":
      document.getElementById("RightHammerToes").textContent,
    "RH_IN:RightInstep": document.getElementById("RightInstep").textContent,
    "RH_IN:RightDepthAdjustment": document.getElementById(
      "RightDepthAdjustment"
    ).textContent,
    "RH_IN:RightHeelWidth":
      document.getElementById("RightHeelWidth").textContent,
    "RH_IN:RightForefootWidth":
      document.getElementById("RightForefootWidth").textContent,
  };
  console.log(data.inputs);
  const request = {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-Type": "application/json" },
  };
  let headers = null;
  try {
    const response = await fetch("/solve", request);
    if (!response.ok) throw new Error(response.statusText);

    headers = response.headers.get("server-timing");
    const responseJson = await response.json();
    // Request finished. Do processing here.
    let t1 = performance.now();
    const computeSolveTime = t1 - timeComputeStart;
    t0 = t1;
    // hide spinner
    //console.log(responseJson);
    document.getElementById("loader").style.display = "none";
    let data = JSON.parse(responseJson.values[0].InnerTree["{0}"][0].data);
    let data2 = JSON.parse(responseJson.values[1].InnerTree["{0}"][0].data);
    mesh = rhino.DracoCompression.decompressBase64String(data);
    console.log(mesh);
    mesh2 = rhino.DracoCompression.decompressBase64String(data2);

    t1 = performance.now();
    const decodeMeshTime = t1 - t0;
    t0 = t1;
    if (!_threeMaterial) {
      _threeMaterial = new THREE.MeshPhysicalMaterial({
        color: 0x49ef4,
        metalness: 0,
        roughness: 1,
        side: THREE.DoubleSide,
      });
    }
    if (!_threeMaterial2) {
      _threeMaterial2 = new THREE.MeshPhysicalMaterial({
        color: 0x10e5dc,
        metalness: 0,
        roughness: 1,
        side: THREE.DoubleSide,
      });
    }
    let threeMesh = meshToThreejs(mesh, _threeMaterial);
    mesh.delete();
    replaceCurrentMesh(threeMesh);
    let threeMesh2 = meshToThreejs(mesh2, _threeMaterial);
    mesh2.delete();
    replaceCurrentMesh2(threeMesh2);
    t1 = performance.now();
    const rebuildSceneTime = t1 - t0;
  } catch (error) {
    console.error(error);
  }
}
// BOILERPLATE //
var scene, camera, renderer, controls, pointlight;
function init() {
  // Rhino models are z-up, so set this as the default
  THREE.Object3D.DefaultUp = new THREE.Vector3(0, 0, 1);
  scene = new THREE.Scene();
  scene.background = new THREE.Color("rgb(245,245,245)");

  renderer = new THREE.WebGLRenderer({
    antialias: true,
    preserveDrawingBuffer: true,
  });
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
  renderer.setSize(500, 250);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.type = THREE.PCFSoftShadowMap;

  document.getElementById("threedwrapper").appendChild(renderer.domElement);

  camera = new THREE.PerspectiveCamera(45, 500 / 250, 1, 2000);
  camera.position.set(-200, 200, 150);
  camera.lookAt(0, 0, 0);

  controls = new OrbitControls(camera, renderer.domElement);
  controls.enabled = false;
  controls.enableDamping = true;

  var ambientLight = new THREE.AmbientLight(0xcccccc, 0.5);
  scene.add(ambientLight);

  const hemisphereLight = new THREE.HemisphereLight(0xff0000, 0x0000ff, 0.7);
  scene.add(hemisphereLight);

  const dirLight = new THREE.DirectionalLight(0xffffff, 0.75);
  dirLight.position.set(0, 0, 300);
  dirLight.castShadow = true;

  dirLight.shadow.mapSize.set(512 * 4, 512 * 4);
  dirLight.shadow.radius = 25;

  dirLight.shadow.camera.left = -500;
  dirLight.shadow.camera.right = 500;
  dirLight.shadow.camera.top = 500;
  dirLight.shadow.camera.bottom = -500;

  dirLight.shadow.camera.near = 1;
  dirLight.shadow.camera.far = 500;

  scene.add(dirLight);

  const planeGeo = new THREE.PlaneGeometry(500, 500, 1, 1);
  const planeMat = new THREE.ShadowMaterial();
  planeMat.opacity = 0.4;

  const plane = new THREE.Mesh(planeGeo, planeMat);
  plane.receiveShadow = true;
  scene.add(plane);

  const gridHelper = new THREE.GridHelper(500, 25, 0xc1c1c1, 0x8d8d8d);
  gridHelper.position.y = -2;
  gridHelper.rotateX(-Math.PI / 2);

  gridHelper.material.transparent = true;
  gridHelper.material.opacity = 0.2;
  scene.add(gridHelper);

  window.addEventListener("resize", onWindowResize, false);
  animate();
}
var animate = function () {
  requestAnimationFrame(animate);
  controls.update();
  renderer.render(scene, camera);
};

function onWindowResize() {
  //camera.aspect = window.innerWidth / window.innerHeight
  //camera.updateProjectionMatrix()
  //renderer.setSize( window.innerWidth, window.innerHeight )
  //animate()
}
function replaceCurrentMesh(threeMesh) {
  if (_threeMesh) {
    scene.remove(_threeMesh);
    _threeMesh.geometry.dispose();
  }
  _threeMesh = threeMesh;
  _threeMesh.castShadow = true;
  scene.add(_threeMesh);
}
function replaceCurrentMesh2(threeMesh) {
  if (_threeMesh2) {
    scene.remove(_threeMesh2);
    _threeMesh2.geometry.dispose();
  }
  _threeMesh2 = threeMesh;
  _threeMesh2.castShadow = true;
  scene.add(_threeMesh2);
}
function meshToThreejs(mesh, material) {
  let loader = new THREE.BufferGeometryLoader();
  var geometry = loader.parse(mesh.toThreejsJSON());
  return new THREE.Mesh(geometry, material);
}
